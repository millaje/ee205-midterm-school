#include <iostream>
#include <string>
#include <cassert>

using namespace std;

namespace school {

class Resume {
public:
	virtual void printResume() = 0;
};


class Person : public Resume {
protected:
	string name;
public:
	Person( string newName );
	~Person();
};

Person::Person ( string newName ) {
	name = newName;
	cout << "Hello "   << name << endl;
}

Person::~Person () {
	cout << "Goodbye " << name << endl;
}


class Professor : public Person {
private:
	short yearOfGraduation;
	short yearHired;
public:
	Professor ( string newName, short graduatedIn, short hired ) : Person ( "Professor " + newName ) {
		yearOfGraduation = graduatedIn;
		yearHired        = hired;
	}
	
	void printResume() {
		cout << name << endl ; 
		cout << "  Year hired: " << yearHired        << endl ;
		cout << "  Graduated: "  << yearOfGraduation << endl ;
	}
};


class Student : public Person {
private:
	float      gpa = 0;
	bool       knows_C_on_Windows = false;
	bool       knows_C_on_Linux   = false;
	static int numberOfStudents;
public:
	Student( string newName, float newGpa ) : Person ( newName ) {
		assert( newGpa >= 0 && newGpa <= 4.0 );
		gpa = newGpa;
		numberOfStudents++;
	}
	
	void knowsC( bool knows_C_on_both ) { 
		knows_C_on_Windows = knows_C_on_both;
		knows_C_on_Linux   = knows_C_on_both; 
	}
	
	void knowsC( bool new_knows_C_on_Windows, bool new_knows_C_on_Linux) {
		knows_C_on_Windows = new_knows_C_on_Windows;
		knows_C_on_Linux   = new_knows_C_on_Linux  ; 
	}

	void printResume() { 
		cout << name << endl;
		cout << "  GPA " << gpa << endl;
		if ( knows_C_on_Windows ) cout << "  Knows C on Windows" << endl;
		if ( knows_C_on_Linux   ) cout << "  Knows C on Linux" << endl;
	}
	
	static int getNumberOfStudents() { return numberOfStudents; }
};

int Student::numberOfStudents = 0;

} // namespace school;


int main() {
	school::Professor teacher = school::Professor ( "Nelson", 1992, 2018 );
	school::Student   sam     = school::Student   ( "Sam"  , 3.2 );
	school::Student   david   = school::Student   ( "David", 2.9 );
	
	sam.knowsC( true );
	david.knowsC( false, true );

	teacher.printResume();
	sam.printResume();
	david.printResume();
	
	cout << "There are " << school::Student::getNumberOfStudents() << " students" << endl;
}

